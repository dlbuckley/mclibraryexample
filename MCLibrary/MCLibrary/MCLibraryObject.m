//
//  MCLibraryObject.m
//  MCLibrary
//
//  Created by Dale Buckley on 04/03/2014.
//  Copyright (c) 2014 Monitise Create. All rights reserved.
//

#import "MCLibraryObject.h"

@interface MCLibraryObject ()

@property (nonatomic, strong) NSArray *array;

@end

@implementation MCLibraryObject

- (id)initWithArray:(NSArray*)array
{
    if (self = [super init]) {
        for (id object in array) {
            NSAssert([object isKindOfClass:[NSString class]], @"Arrat should contain NSString objects only");
        }
        self.array = array;
    }
    return self;
}

- (void)sortArrayAlphabetically
{
    self.array = [self.array sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
}

- (void)removeEmptyElements
{
    NSMutableArray *mutableArray = [NSMutableArray arrayWithArray:self.array];
    
    [mutableArray removeObject:@""];
    //[mutableArray removeObject:@" "]; <--- Make Test Pass
    
    self.array = [NSArray arrayWithArray:mutableArray];
}

@end
