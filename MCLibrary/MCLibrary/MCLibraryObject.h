//
//  MCLibraryObject.h
//  MCLibrary
//
//  Created by Dale Buckley on 04/03/2014.
//  Copyright (c) 2014 Monitise Create. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MCLibraryObject : NSObject

@property (nonatomic, strong, readonly) NSArray *array;

/*!
 Initialise an MCLibraryObject with an array of string objects
 @param array An initialised array of NSString objects
 @return An initialised MCLibraryObject
 */
- (id)initWithArray:(NSArray*)array;

/*!
 Sort the string objects in the array into alphabetical order.
 */
- (void)sortArrayAlphabetically;

/*!
 Remove any string elements in the array that are empty.
 */
- (void)removeEmptyElements;

@end
