//
//  MCLibraryTests.m
//  MCLibraryTests
//
//  Created by Dale Buckley on 04/03/2014.
//  Copyright (c) 2014 Monitise Create. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "MCLibraryObject.h"

@interface MCLibraryTests : XCTestCase

@property (nonatomic, strong) MCLibraryObject *libraryObject;
@property (nonatomic, strong) NSArray *array;

@end

@implementation MCLibraryTests

- (void)setUp
{
    [super setUp];
    
    self.array = @[@"Dan",
                   @"Valerio",
                   @"Simon",
                   @"Kseniya",
                   @"Sam",
                   @"Chris",
                   @"Mike",
                   @"Victor",
                   @"Dale",
                   @"Delroy",
                   @"Rob",
                   @"Adam",
                   @"Ryan"];
    
    self.libraryObject = [[MCLibraryObject alloc] initWithArray:[self.array copy]];
}

- (void)tearDown
{
    self.libraryObject = nil;
    
    [super tearDown];
}

- (void)testArrayMatchesArray
{
    for (int i = 0; i < self.array.count; i++) {
        XCTAssertTrue([self.array[i] isEqual:self.libraryObject.array[i]], @"An Element doesn't match in the arrays!");
    }
}

- (void)testAlphabeticalSort
{
    NSArray *array = @[@"Adam",
                       @"Chris",
                       @"Dale",
                       @"Dan",
                       @"Delroy",
                       @"Kseniya",
                       @"Mike",
                       @"Rob",
                       @"Ryan",
                       @"Sam",
                       @"Simon",
                       @"Valerio",
                       @"Victor"];
    
    [self.libraryObject sortArrayAlphabetically];
    
    XCTAssertTrue([array isEqual:self.libraryObject.array], @"The arrays order doesn't match %s", __PRETTY_FUNCTION__);
}

- (void)testRemovalOfEmptyElements
{
    NSMutableArray *mutableArray = [NSMutableArray arrayWithArray:[self.array copy]];
    
    [mutableArray addObject:@""];
    //[mutableArray addObject:@" "]; <--- Make test fail
    
    self.libraryObject = [[MCLibraryObject alloc] initWithArray:mutableArray];
    
    XCTAssertTrue([mutableArray isEqual:self.libraryObject.array], @"MCLibraryObject not initialised correctly %s", __PRETTY_FUNCTION__);
    
    [self.libraryObject removeEmptyElements];
    
    XCTAssertTrue([self.array isEqual:self.libraryObject.array], @"Empty objects not removed correctly %s", __PRETTY_FUNCTION__);
}

@end
