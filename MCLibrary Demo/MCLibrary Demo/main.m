//
//  main.m
//  MCLibrary Demo
//
//  Created by Dale Buckley on 04/03/2014.
//  Copyright (c) 2014 Monitise Create. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MCAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([MCAppDelegate class]));
    }
}
