//
//  MCLibrary_DemoTests.m
//  MCLibrary DemoTests
//
//  Created by Dale Buckley on 04/03/2014.
//  Copyright (c) 2014 Monitise Create. All rights reserved.
//

#import <XCTest/XCTest.h>

@interface MCLibrary_DemoTests : XCTestCase

@end

@implementation MCLibrary_DemoTests

- (void)setUp
{
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testExample
{
    XCTFail(@"No implementation for \"%s\"", __PRETTY_FUNCTION__);
}

@end
